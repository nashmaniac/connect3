package com.nashmaniac.connect3;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public int activePlayer = 0;
    public int [] gameStates={2,2,2,2,2,2,2,2,2};
    public int[][] winningStates = {
            {0, 1, 2},
            {3, 4, 5},
            {6, 7, 8},
            {0, 3, 6},
            {1, 4, 7},
            {2, 5, 8},
            {0, 4, 8},
            {2, 4, 6}
    };
    public void resetBoard(View view){
        activePlayer = 0;
        for(int i=0;i<gameStates.length;i++){
            gameStates[i]=2;
        }
        GridLayout grid =(GridLayout) findViewById(R.id.boardGrid);
        for(int i=0;i<grid.getChildCount();i++){
            ((ImageView) grid.getChildAt(i)).setImageResource(0);
        }
    }
    public void buttonHit(View view){
        // 0 = yellow 1 = red
        ImageView imageView = (ImageView) view;
        int tag = Integer.parseInt(imageView.getTag().toString());
        if(gameStates[tag]==2){
            imageView.setTranslationY(-1000f);
            gameStates[tag] = activePlayer;
            if(activePlayer==0){
                imageView.setImageResource(R.drawable.yellow);
                activePlayer = 1;
            }else{
                imageView.setImageResource(R.drawable.red);
                activePlayer = 0;
            }
            imageView.animate().translationYBy(1000f).setDuration(1000);
            for(int[] eachState: winningStates){
                if(gameStates[eachState[0]]==gameStates[eachState[1]] && gameStates[eachState[1]]==gameStates[eachState[2]] && gameStates[eachState[2]]==gameStates[eachState[0]]){
                    if(gameStates[eachState[0]]!=2){
                        Toast.makeText(MainActivity.this,"Player "+ String.valueOf(gameStates[eachState[0]]+1)+" wins.", Toast.LENGTH_LONG).show();
                        break;
                    }else{
                        continue;
                    }
                }else{
                    continue;
                }
            }
        }else{
            Toast.makeText(MainActivity.this, "Already Played.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
